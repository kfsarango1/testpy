#!/usr/bin/python
# -*- coding: utf-8 -*-

import csv
import time
import requests
import json



def uniques():
	output = []
	with open('src/kw_es_en.csv', mode='r') as csv_file:
		csv_reader = csv.DictReader(csv_file)
		for row in csv_reader:
			typeData = row['Part']
			if typeData not in output:
				output.append(typeData)

		print (output)				

def findMatch(valor):
	with open('src/kw_es_en.csv', mode='r') as csv_file:
		csv_reader = csv.DictReader(csv_file)
		line_count = 0
		for row in csv_reader:
			typeData = row['Part']
			if typeData == valor:
				line_count = line_count + 1

		return line_count				



def countSin():
	with open('src/sin.csv', mode='r') as csv_file:
		csv_reader = csv.DictReader(csv_file)
		sin_count = 0
		for x in csv_reader:
			row = x['Frase']
			tocheck = row[:3]
			if tocheck == 'Sin':
				lstData = row.split(',')
				sin_count = sin_count + len(lstData)

		print (sin_count)


#########################################################

def findNewIdx(thelist, now):
	now = now + 1
	for j in thelist[now:]:
		if j['if_word'] == 'Word':
			return thelist.index(j)

def toLastRowWithText(thelistA):
	for a in thelistA:
		if a['frase'] == '':
			return thelistA.index(a)

def printBoxData(thelist, start, end, myid):

	newListAux = thelist[start:end]
	print (myid)
	rowAtLastField = toLastRowWithText(thelist)
	for x in newListAux[:1]:
		col1 = x['if_word']
		col2 = x['word']
		col3 = x['phonema']
		col4 = x['tipo']
		col5 = x['toponimo']
		col6 = x['significado']
		col7 = x['frase']
		
		print (col7)

	sinonims = ''
	
	#Encontrando sinonimo
	for sn in newListAux:
		tocheckSin = sn['frase']
		if tocheckSin[:3] == 'Sin':
			sinonims = tocheckSin[5:]
			print ('SIN: '+sinonims)
	
	#Haciendo los inserts
	data = {
			"id": 1,
		    "kichwa": col2, 
		    "phonema": col3, 
		    "tipo": col4,
			"toponimo": col5,
		    "sinonimos": sinonims,
		    "castellano": col6,
		}
	data_json = json.dumps(data)
	headers = {'Content-type': 'application/json'}

	response = requests.post('http://127.0.0.1:4567/addkichwacastellano', data=data_json, headers=headers)

	print ('EJEMPLOS')
	for y in newListAux[1:]:
		ejemplo = y['frase']
		if ejemplo != '' and ejemplo[:3] != 'Sin':
			print (ejemplo)
			example = {
			"id": 1,
		    "detalle": ejemplo, 
		    "idkw_cas": myid
				}

			data_json = json.dumps(example)

			headers = {'Content-type': 'application/json'}

			response = requests.post('http://127.0.0.1:4567/addejemplos_kwcas', data=data_json, headers=headers)
	
	print ('Saved!..\n\n')

		

def getListInit():
	with open('src/kw_cas_complet.csv', mode='r') as csv_file:
		csv_reader = csv.DictReader(csv_file)
		listCsv =  list(csv_reader)

		return listCsv

def findValuesKW_CAS(arrayl,idxStart, idd):
	if idxStart < len(arrayl):
		try:
			for x in arrayl[idxStart:]:
				newIdx = findNewIdx(arrayl, arrayl.index(x))
				#print listCsv[newIdx]['word']
				printBoxData(arrayl,idxStart,newIdx, idd)
				findValuesKW_CAS(arrayl, newIdx, idd + 1)
				return
		except Exception as e:
			print (e)
			print('indextoStart: >', idxStart)
			print('id: >', idd)
	else:
		print('END ..')

			

#############################################################

#valuesUnique = ['adv.', 's.', 'adj.', 'v.', 'preg.', '-o, -a, det.', 'det.', '-a, adj.', '-a, s.', 'w.', 'pron.', 'interj.', 'conj.', 'num.', 'comp.', 'expr.', '-a, v.', 'exp.', 'mon.', 'expresi\xc3\xb3n.', 'onom.']

#valuesUnique = ['interj.', 'adv.', 's.', 'adj.', 'v.', 'det.', 'num.', 'adv s.', 'exp.', 'pron.', '(<*kiw\xc3\xb1a) s.', 'onom.', 'interrog.', 'comp.', 's.s.', 'sc.', 'conj.'] 

#valuesUnique = ['verb; noun', 'noun', 'adv', 'verb', 'adj', 'verb; adv', 'verb; adj', 'adj; verb', 'verb; adj; noun', 'noun; verb', 'noun; adj', 'adj; verb; noun', 'adj; noun', 'noun; adj; verb', 'adj; verb; adv', 'adj; adv', 'adj; noun; adv', 'adj; adv; noun', 'adj; noun; verb', 'noun; verb; adj', 'adverb; conjunction', 'adv; adj', 'noun; interj', 'noun; noun', 'interj; noun', 'prepo; adv; conj', 'adv; adj; noun', 'adv; adj; pron', 'adv; prop', 'veb', 'verb; noun; interj', 'prep; adv; adj', 'prep.', 'verb; verb', 'verb; noun; adv', 'noun; vereb', 'adv; noun', 'noun; adj; adv', 'conj; pron', 'prep; adv', 'pronoun', 'adv; noun; interj', 'pron', 'noun; adv', 'adv; conj', 'adv; noun; adj', 'prep', 'noun; adv; adj', 'adv; conj; noun', 'prep;adj; adv', 'adj; noun; verb; adv', 'adj; interj; pron', 'adj ; adv', 'adj; pronoun; noun', 'verb; adj; adv', 'prep; adj', 'conj; prep; adv', 'conj; adv', 'adj; verb; prep', 'pron; interj; adv', 'pronoun; adv', 'verb; noun; adj', 'pron; adj; interjection', 'pron; adv; interject', 'pron; adv', 'pron; adj; conjun', 'noun; interjection', 'noun; verb;', 'verb; adj; adj', 'prep; conj', 'possessive pron', 'pronoun possessive', 'adv; noun; prepos; adj', 'adj; noun; prepo', 'adj; pron', 'adj; verb; verb', 'nou', 'prop', 'adv; prep', 'noun;adj', 'noun;', 'adv; adj; verb', 'verb; interj; adj', 'vereb; noun', 'conj', 'conjunc', 'adv; prepo', 'adv; conjunction', 'preposition', 'prop; adv', 'contrac', 'noun; conj; verb', 'adv; conj; adj; pron', 'pronoun; noun; adv', 'pron. plural', 'adj;', 'verb; noun; interj.', 'noun; adv; verb', 'noun;verb', 'pronoun; noun', 'adj.', 'interr pronoun; noun', 'verb; noun; adv; adj; prep', 'noun; verbs', 'verb; adv; noun; adj', 'verb; prep; conj', 'noun; adj.', 'noun; verb;adj.', 'prepos; adv.', 'verb; noun; interjection', 'adj; adv; verb; prepos', 'verb;', 'adj; adv; noun; verb', 'noun; verb; adj; adv', 'nou; verb', 'noun; ver', 'noun; verb; interjec.', 'verb; interjec', 'adv; adj; pronoun', 'veerb; noun', 'adj; adv; verb', 'adj; noun; prep', 'noun, prep.', 'adv; interj', 'noun; objec', 'verb; interkj; noun', 'adj; adv; pronoun', 'adverb', 'noun; block', 'prepo; adj; adv', 'prepo', 'noun; verb; interj', 'adj; prepo; adv', 'noon', 'noun; rage', 'verb;interj', 'pron; noun', 'verb; noun; conj', 'verb; noun;', 'noun; adj; interj', 'noun; anger', 'adv; prepo; conj']

'''
for val in valuesUnique:
	print 'To ' + val + ':'
	repeats =  findMatch(val)
	print '\t',repeats '''
myListData = getListInit()

findValuesKW_CAS(myListData, 11258, 1931)







  