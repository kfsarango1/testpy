#Author @kfsarango1

import sys
from mpi4py import MPI

size = MPI.COMM_WORLD.Get_size()

rank = MPI.COMM_WORLD.Get_rank()

name = MPI.Get_processor_name()

sys.stdout.write(
    "Pase de mensajes MPI. Run at process %d of %d on %s.\n" % (rank, size, name))