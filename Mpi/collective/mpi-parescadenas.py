#!/usr/bin/env python

#Charlie Alexander Cárdenas Toledo
from mpi4py import MPI
import sys
from random import randrange
comm = MPI.COMM_WORLD
size = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
	msg = 'UTPL'
	print ("El proceso %d de %d procesos envía una cadena: %s.\n"
	% (rank, size, msg))
	comm.send(msg, dest=3, tag=1)
if rank == 1:
	a = randrange(0,100,2)
	b = randrange(0,100,2)
	total = a + b
	print ("El proceso %d de %d procesos envía la suma total de %d + %d.\n"
	% (rank, size, a, b))
	comm.send(total, dest=3, tag=2)
elif rank == 3:
	string = comm.recv(tag=1)
	number = comm.recv(tag=2)
	print ("El proceso %d: recibe una cadena ---> %s y la suma %d" % (rank, string, number))
#else:
#	print ("El proceso %d de %d procesos.\n"#
#	% (rank, size))
