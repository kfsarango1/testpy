'''
+------------------+
|   TAREA 2 MPI    |
|by Klever Sarango |
+------------------+
'''
from mpi4py import MPI
import sys
import random

comm = MPI.COMM_WORLD
size = MPI.COMM_WORLD.Get_size()
rank = MPI.COMM_WORLD.Get_rank()

if rank == 0:
	msg = 'UTPL'
	print ("El proceso %d de %d procesos ENVIA una cadenta: %s.\n" % (rank, size, msg))
	#enviando mensaje
	comm.send(msg, dest=3, tag=1)

elif rank == 1:
	#generando numeros aleatorios
	n1 = random.randint(1,101)
	n2 = random.randint(1,101)
	sumary = n1 + n2

	print ("El proceso %d de %d procesos ENVIA la total de %d + %d.\n" % (rank, size, n1, n2))
	#enviando mensaje
	comm.send(sumary, dest=3, tag=2)

elif rank == 3:
	#recibiendo los parametros enviados anteriormente
	firstVal = comm.recv(tag=1)
	secondVal = comm.recv(tag=2)
	print ("El proceso %d: RECIBE una cadena: %s" % (rank, firstVal))
	print ("El Proceso %d: RECIBE una total de dos numeros %s" % (rank, secondVal))
else:
	print ("El proceso %d de %d procesos.\n" % (rank, size))

'''
OUTPUT
El proceso 0 de 4 procesos ENVIA una cadenta: UTPL.
El proceso 1 de 4 procesos ENVIA la total de 90 + 70.

El proceso 2 de 4 procesos.


El proceso 3: RECIBE una cadena: UTPL
El Proceso 3: RECIBE una total de dos numeros 160
'''