import numpy
from math import acos, cos
from mpi4py import MPI
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()

pi = 3.14159265359
a = 0.0
b = pi / 2.0
dest = 0
my_int = numpy.zeros(1)
print my_int
print 'endt ints'
integral_sum = numpy.zeros(1)
print integral_sum

# Se inicializa el valor de n, solo si estamos en el proceso 0
if rank == 0:
    n = numpy.full(1, 500, dtype=int) # default value
else:
    n = numpy.zeros(1, dtype=int)

# Broadcast n a todos los procesos
print("Proceso ", rank, " despues n = ", n[0])
comm.Bcast(n, root=0)
print("Proceso ", rank, " antes n = ", n[0])

# Compute partition
h = (b - a) / (n * size) # calculamos h *despues* se recibe n
my_int[0] = h

# envia la particion al proceso raiz, calculando la suma en todas las particiones
print("Proceso ", rank, " tiene la integral parcial ", my_int[0])
comm.Reduce(my_int, integral_sum, MPI.SUM, dest)

# Only print the result in process 0
if rank == 0:
    print('La suma de la integral =', integral_sum[0])