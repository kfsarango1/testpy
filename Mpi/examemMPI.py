from mpi4py import MPI
import random
'''
+------------------+
|      EXAMEN      |
|by Klever Sarango |
+------------------+
Genere los factoriales de un numero, llenar en un arreglo
Enviar y recibir en un mensaje mediante la funcion Bcast
'''


#variables MPI
comm = MPI.COMM_WORLD
rank = comm.rank

if rank == 0:
	#Generando factoriales y llenado el arreglo
	data = []

	factorial_total = 1
	n = 5
	while n > 1:
		factorial_total = n * factorial_total
		n = n - 1
		data.append(n)
	#data.append(factorial_total)

else:
    data = None

#enviando mediante la funcion broadcast
data = comm.bcast(data, root=0)
print 'En la  tarea ',rank,'aplicado bcast:    data = ',data	
