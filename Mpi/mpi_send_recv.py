#Realizado por Klever Sarango

import sys
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank() #Identificador del proceso que lo llama
size = MPI.COMM_WORLD.Get_size() #numero de procesos

if rank == 0:
    data = 'Probando el envio y recibido de strings con MPI'
    comm.send(data, dest=1)
elif rank == 1:
    data = comm.recv(source=0)
    print("Desde el proceso %d: recibiendo data para el proceso 0 ---> %s" % (rank, data))
else: 
    
	sys.stdout.write(
    	"Pase de mensajes MPI. Ejecutando en el proceso %d de %d.\n" % (rank, size))
